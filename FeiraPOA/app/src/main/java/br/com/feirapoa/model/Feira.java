package br.com.feirapoa.model;

/**
 * Created by Rafael on 22/04/2016.
 */
public class Feira {

   public String titulo;
    public String endereco;
    public String horario;
    public String localizacao;

    public Feira(String titulo, String endereco, String horario, String localizacao) {
        this.titulo = titulo;
        this.endereco = endereco;
        this.horario = horario;
        this.localizacao = localizacao;
    }

    public Feira(String titulo, String endereco, String horario ) {
        this.titulo = titulo;
        this.endereco = endereco;
        this.horario = horario;
    }
    public Feira() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    @Override
    public String toString() {
        return "Feira{" +
                "titulo='" + titulo + '\'' +
                ", endereco='" + endereco + '\'' +
                ", horario='" + horario + '\'' +
                ", localizacao='" + localizacao + '\'' +
                '}';
    }
}
