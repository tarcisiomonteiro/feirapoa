package br.com.feirapoa.service;

import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;


public class PUTService extends AsyncTask {

    private int PERIOD = 1500;
    private boolean run = true;
    URL url = null;

    public void kill() {
        this.run = false;
    }

    @Override
    protected Void doInBackground(Object... params) {

        while (run) {
            try {
                Thread.sleep(PERIOD);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                url = new URL("https://diaristas.herokuapp.com/api/evolucao");
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
                httpCon.setDoOutput(true);
                httpCon.setRequestMethod("POST");

                OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
                out.write("Resource content");
                out.close();

                InputStream inputStream = httpCon.getInputStream();
                int input = httpCon.getResponseCode();
                Log.d("Mapa", "OK");

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("Hummm", "not good");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}