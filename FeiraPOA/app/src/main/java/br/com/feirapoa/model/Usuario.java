package br.com.feirapoa.model;

/**
 * Created by Rafael on 19/04/2016.
 */
public class Usuario {

    private String nome;
    private String username;
    private String cidade;

    public Usuario(String nome, String username, String cidade) {
        this.nome = nome;
        this.username = username;
        this.cidade = cidade;
    }

    public Usuario() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nome='" + nome + '\'' +
                ", username='" + username + '\'' +
                ", cidade='" + cidade + '\'' +
                '}';
    }
}
