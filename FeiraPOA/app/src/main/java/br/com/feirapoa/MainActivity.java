package br.com.feirapoa;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.feirapoa.model.Feira;

public class MainActivity extends AppCompatActivity {

    private List<Feira> listaFeiras = new ArrayList<>();
    private RecyclerView recyclerView;
    private FeirasAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new FeirasAdapter(listaFeiras);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Feira feira = listaFeiras.get(position);
                Toast.makeText(getApplicationContext(), feira.getTitulo(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        prepareFeiras();

    /*    if(savedInstanceState != null){
            MapsActivity mapaFragment = new MapsActivity();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragLayout, mapaFragment).commit();
        }*/
    }

    private void prepareFeiras() {

        Feira feira = new Feira("Feira Modelo Epatur",
                "Largo Zumbi dos Palmares", "3ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo General João Telles",
                "Rua Geneneral João Telles, Bairro Bom Fim.", "3ª feira, das 7:00 às 12:00");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Guido Mondim",
                "Av. Guido Mondim esquina com a Presidente Roosevelt, Bairro São Geraldo.",
                "4ª feira, das 7:00 às 12:00");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo São Pedro",
                "Largo 1º de Junho, 362 (em frente ao Zivi Hércules), Bairro Santa Maria Goretti.",
                "4ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira(
                "nome:" + "Feira Modelo Camaquã",
                "Rua Prof. Dr. João Pita Pinheiro Filho, 49 (entre as ruas Pereira Neto e Mário Totta), Bairro Camaquã.",
                "4ª feira, das 15:30 às 20:30.");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo Terminal Alameda",
                "Rua Cel. José Rodrigues Soal, 958 (esquina com a Av. Luiz Moschetti), Vila João Pessoa.",
                "4ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo Teresópolis",
                "Rua José Carlos Ferreira, em frente ao nº 325 (esquina com a R. Arnaldo Bohrer), Bairro Teresópolis",
                "4ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo Cristal",
                "Rua Jaguari, esquina com a Avenida Campos Velho, Bairro Cristal.",
                "4ªfeira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira(  "Feira Modelo Jardim Leopoldina",
                "Rua Dr. Carlos Maria Bins, entre a Av. Baltazar de Oliveira Garcia e a Rua Carlos Estevão, no Bairro Jardim Leopoldina.",
                "4ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Parque dos Maias",
                "Av. Bernardino Silveira de Amorim, 3330 (esquina com a Rua dos Maias), Bairro Rubem Berta.",
                "5ª feira, das 15:30 às 20:30.");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Jardim Itú",
                "Av. Professora Paula Soares, 1822 (esquina com R. Gaspar de Lemos), Jardim Itú Sabará.",
                "5ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Morro Santana",
                "Rua Amadeu Fagundes de Oliveira Freitas, 35 (esquina com a Avenida Protásio Alves), Morro Santana.",
                "5ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Ecológica Bom Jesus",
                "Rua São Felipe, 140 (esquina das ruas Dr. Murtinho e Bom Jesus), Bom Jesus.",
                "5ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Humaitá",
                "Rua Professor João de Souza Ribeiro, 707 (entre a Av. Engº Felício Lemieszek e Rua Bonifácio Nunes), Humaitá",
                "5ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo Jardim Ypu",
                "Av. Germano Schermarckzec (entre as ruas Hugo Livi e Mãe Apolinária Matias Batista), Jardim Ypu",
                "5ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo Menino Deus",
                "Praça Israel - Rua Vicente Lopes Santos (esquina com a Rua Botafogo), Bairro Menino Deus.",
                "5ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo Barão do Triunfo",
                "Rua Barão do Triunfo (esquina com a Rua Vinte de Setemo), Azenha",
                "6ª feira, das 7:00 às 12:00");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Sarandi",
                "Praça Lampadosa - Av. 21 de Ail, 792, Bairro Sarandi",
                "6ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira( "Feira Modelo Jardim Ypu",
                "Av. Germano Schermarckzec (entre as ruas Hugo Livi e Mãe Apolinária Matias Batista), Jardim Ypu",
                "5ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Costa e Silva",
                "Av. Silvestre Felix Rodrigues (próximo a Rua Ivan Gatti), Residencial Costa e Silva",
                "6ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Jardim Botânico",
                "Rua Felizardo Furtado, ao lado da ESEF, Jardim Botânico.",
                "6ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Marinha do asil",
                "No Parque Marinha do asil, na Av. Padre Cacique, 398 (esquina com a Rua Miguel Couto), Praia de Belas",
                "6ª feira, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Jardim do Salso",
                "Rua Prof. Abílio Azambuja, 456 (esquina com a R. São Benedito), Jardim do Salso",
                "Sábado, das 7:30 às 12:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Passo da Mangueira",
                "Rua Francisco Rodolfo Sinck, Passo da Mangueira.",
                "Sábado, das 7:30 às 12:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Hípica",
                "Rua Luzinete Alves Aragon",
                "Sábados, das 7:30 às 12:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Partenon PUC-RS",
                "Avenida Ceres (esquina com a R. Frei Germano e em frente a Praça Padre Neídio Bolcato), Partenon",
                "Sábado, das 7:30 às 12:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Juca Batista",
                "Av. Juca Batista, 1635 (em frente à Igreja Menino Jesus de Praga), Aberta dos Morros",
                "Sábado, das 7:30 às 12:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Fernando Machado",
                "Rua Cel. Fernando Machado, esquina R. Gen. Bento Martins, Centro",
                "Sábado, das 7:30 às 12:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Vila Ipiranga",
                "Av. Dr. João Simplício Alves de Carvalho, Vila Ipiranga.",
                "Sábado, das 7:30 às 12:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Bom Fim",
                "Rua Irmão José Otão, 438 (entre R. João Telles e R. Santo Antônio), Bom Fim",
                "Sábado, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Restinga",
                "Estrada João Antônio da Silveira, 2355 Esplanada da Restinga",
                "Sábado, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Pedro Souza",
                "Rua Pedro Souza, esquina R. Malet, Bairro São José",
                "Sábado, das 15:30 às 20:30");
        listaFeiras.add(feira);

        feira = new Feira("Feira Modelo Ceará",
                "Av. Ceará, esquina com a R. João Inácio, São João",
                "Domingo, das 7:30 às 12:30");
        listaFeiras.add(feira);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sobre) {
            mostraAbout();
            return true;
        } else if(id == R.id.action_mapa){
            overridePendingTransition(R.anim.fadein, R.anim.fadeout);
       /* overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
            startActivity(new Intent(getBaseContext(), MapsActivityNew.class));
            Toast.makeText(getApplicationContext(), "Localizando feiras...", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private MainActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final MainActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public void mostraMapa(View view) {
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
       /* overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
        startActivity(new Intent(getBaseContext(), MapsActivityNew.class));
        Toast.makeText(getApplicationContext(), "Localizando feiras...", Toast.LENGTH_LONG).show();
    }

    public void mostraAbout(){
       /* overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
        startActivity(new Intent(getBaseContext(), AboutActivity.class));
        Toast.makeText(getApplicationContext(), "Sobre nós.", Toast.LENGTH_SHORT).show();
    }

    //Authorization code for Marshmallow version.
    private void requestSinglePermission() {
        int REQUEST_LOCATION = 1503;
        String locationPermission = Manifest.permission.ACCESS_FINE_LOCATION;
        int hasPermission = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasPermission = checkSelfPermission(locationPermission);
        }
        String[] permissions = new String[]{locationPermission};
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, REQUEST_LOCATION);
            }
        } else {
            // Phew - we already have permission!
        }
    }
}
