package br.com.feirapoa.service;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.feirapoa.model.Feira;

/**
 * Created by Rafael on 24/04/2016.
 */
public class POST {

    public static void postNewDevice(final Context context, final Feira feira) {

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "https://diaristas.herokuapp.com/api/device";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response).getJSONObject("device");
                    String site = jsonResponse.getString("deviceId");
                    String network = jsonResponse.getString("perfil");
                    Log.d("Device", "Posted!");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                 String android_id = Settings.Secure.getString(context.getApplicationContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("Id", android_id);
                params.put("Perfil", "Diarista");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };

        Volley.newRequestQueue(context).add(postRequest);
    }
}

