package br.com.feirapoa;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.feirapoa.model.Feira;

/**
 * Created by Rafael on 23/04/2016.
 */
public class FeirasAdapter extends RecyclerView.Adapter<FeirasAdapter.MyViewHolder>  {
    private List<Feira> listaFeiras;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titulo, endereco, horario;

        public MyViewHolder(View view) {
            super(view);
            titulo = (TextView) view.findViewById(R.id.titulo);
            endereco = (TextView) view.findViewById(R.id.endereco);
            horario = (TextView) view.findViewById(R.id.horario);
        }
    }

    public FeirasAdapter(List<Feira> listaFeiras){
        this.listaFeiras = listaFeiras;
    }

    @Override
    public FeirasAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.feira_lista, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FeirasAdapter.MyViewHolder holder, int position) {

        Feira feira = listaFeiras.get(position);
        holder.titulo.setText(feira.getTitulo());
        holder.endereco.setText(feira.getEndereco());
        holder.horario.setText(feira.getHorario());
    }

    @Override
    public int getItemCount() {
        return listaFeiras.size();
    }
}
