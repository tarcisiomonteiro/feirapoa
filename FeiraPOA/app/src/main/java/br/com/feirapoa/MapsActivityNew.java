package br.com.feirapoa;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import br.com.feirapoa.service.PUTService;
public class MapsActivityNew extends FragmentActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public String deviceId;
    private GoogleMap mMap;
    private Context mContext;
    private LocationSource.OnLocationChangedListener mListener;
    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_activity_new);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    //Authorization code for Marshmallow version.
    private void requestSinglePermission() {
        int REQUEST_LOCATION = 1503;
        String locationPermission = Manifest.permission.ACCESS_FINE_LOCATION;
        int hasPermission = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasPermission = checkSelfPermission(locationPermission);
        }
        String[] permissions = new String[]{locationPermission};
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, REQUEST_LOCATION);
            }
        } else {
            // Phew - we already have permission!
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onPause() {
        /* Disable the my-location layer (this causes our LocationSource to be automatically deactivated.) */
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            requestSinglePermission();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            mMap.setMyLocationEnabled(true);

        }
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // Add a marker in senac and move the camera
        LatLng myLocation = new LatLng(-30.029520, -51.181083);
        LatLng epatur = new LatLng(-30.038345, -51.226886);
        LatLng jtelles = new LatLng(-30.033793, -51.212688);
        LatLng guido = new LatLng(-30.009336, -51.203408);
        LatLng mercadao = new LatLng(-30.037425, -51.225989);
        LatLng md = new LatLng(-30.056555, -51.2225);
        LatLng saopedro = new LatLng(-30.007692, -51.168458);
        LatLng camaqua = new LatLng(-30.107148, -51.236157);
        LatLng almeida = new LatLng(-30.068335, -51.176655);
        LatLng teresopolis = new LatLng(-30.082395000000005, -51.204368);
        LatLng cristal = new LatLng(-30.092081, -51.236796);
        LatLng leopoldina = new LatLng(-30.016207000000005, -51.113865);
        LatLng maias = new LatLng(-30.005480999999996, -51.097734);
        LatLng itu = new LatLng(-30.026533999999998, -51.142795);
        LatLng santana = new LatLng(-30.039660999999995, -51.126962);
        LatLng bomjesus = new LatLng(-30.042582000000003, -51.154098);
        LatLng humaita = new LatLng(-29.979472000000005, -51.190442000000004);
        LatLng ypu = new LatLng(-30.046322, -51.135652);
        LatLng menino = new LatLng(-30.053305, -51.225801);
        LatLng triunfo = new LatLng(-30.052826, -51.214928);
        LatLng sarandi = new LatLng(-30.039660999999995, -51.126962);
        LatLng leop2 = new LatLng(-30.02002, -51.114342);
        LatLng costasilva = new LatLng(-30.008899999999997, -51.11894);
        LatLng jdbton = new LatLng(-30.050050000000002, -51.184149);
        LatLng asil = new LatLng(-30.062954, -51.23171);
        LatLng salso = new LatLng(-30.047921, -51.166192);
        LatLng hipica = new LatLng(-30.159187, -51.188704);
        LatLng mangueira = new LatLng(-30.006912, -51.131004);
        LatLng partenon = new LatLng(-30.056988, -51.167415);
        LatLng juca = new LatLng(-30.145869, -51.211846);
        LatLng fernando = new LatLng(-30.034910000000004, -51.233888);
        LatLng vipiranga = new LatLng(-30.019366, -51.158424);
        LatLng bomfim = new LatLng(-30.031502, -51.213375);
        LatLng restinga = new LatLng(-30.154393, -51.138246);
        LatLng pedrosouza = new LatLng(-30.064949000000002, -51.171666);
        LatLng ceara = new LatLng(-30.00289, -51.192099);
        LatLng santarosa = new LatLng(-29.995853, -51.10043);
        LatLng vjardim = new LatLng(-30.034908, -51.154618);
        LatLng cruzeiro = new LatLng(-30.073183000000004, -51.218943);
        LatLng nonoai = new LatLng(-30.090386, -51.225262);
        LatLng tristeza = new LatLng(-30.110303999999996, -51.253763);
        LatLng bonifacio = new LatLng(-30.03682, -51.211111);
        LatLng ecopetro = new LatLng(-30.049586999999995, -51.195018);
        LatLng stoantonio = new LatLng(-30.053543, -51.204223);
        LatLng medianeira = new LatLng(-30.059929, -51.216336);
        LatLng puc = new LatLng(-30.056988, -51.167415);
        LatLng sebastiao = new LatLng(-30.004788999999995, -51.145705);
        LatLng cavalhada = new LatLng( -30.101517000000005, -51.229371);
        LatLng prodipiranga = new LatLng(-30.010927, -51.146142);
        LatLng tresfigueiras = new LatLng(-30.03099, -51.168727);
        LatLng prodcidadeb = new LatLng(-30.038229, -51.226239);


        CameraUpdate update = (CameraUpdateFactory.newLatLngZoom(myLocation, 12));
        mMap.animateCamera(update, 4000, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(), "Localização encontrada.", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Sinal de GPS perdido", Toast.LENGTH_LONG).show();
            }
        });

        mMap.addMarker(new MarkerOptions().position(myLocation)
                .title("PORTO ALEGRE")
                .snippet("Feiras"));

        mMap.addMarker(new MarkerOptions().position(itu)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .title("Feira Modelo Jardim Itú")
                .snippet("5ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(epatur)
                .title("Feira Modelo Epatur")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("3ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(jtelles)
                .title("Feira Modelo General João Telles")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("3ª feira, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(guido)
                .title("Feira Modelo Guido Mondim")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ª feira, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(mercadao)
                .title("Mercadão do Produtor - Cidade Baixa")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(md)
                .title("Feira Ecológica Menino Deus")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ª feira, das 13:00 às 19:00. " +
                        "Sábado, das 7:00 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(saopedro)
                .title("Feira Modelo São Pedro")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(camaqua)
                .title("Feira Modelo Camaquã")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ª feira, das 15:30 às 20:30."));

        mMap.addMarker(new MarkerOptions().position(almeida)
                .title("Feira Modelo Terminal Alameda")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(teresopolis)
                .title("Feira Modelo Teresópolis")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(cristal)
                .title("Feira Modelo Cristal")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ªfeira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(leopoldina)
                .title("Feira Modelo Jardim Leopoldina")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("4ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(maias)
                .title("Feira Modelo Parque dos Maias")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("5ª feira, das 15:30 às 20:30."));

        mMap.addMarker(new MarkerOptions().position(santana)
                .title("Feira Modelo Morro Santana")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("5ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(bomjesus)
                .title("Feira Ecológica Bom Jesus")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("5ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(humaita)
                .title("Feira Modelo Humaitá")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("5ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(ypu)
                .title("Feira Modelo Jardim Ypu")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("5ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(menino)
                .title( "Feira Modelo Menino Deus")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("5ª feira, das 15:30 às 20:30"));


        mMap.addMarker(new MarkerOptions().position(menino)
                .title( "Feira Modelo Menino Deus")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("5ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(triunfo)
                .title("Feira Modelo Barão do Triunfo")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("6ª feira, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(sarandi)
                .title( "Feira Modelo Sarandi")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("6ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(leop2)
                .title( "Feira Modelo Jardim Leopoldina II")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("6ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(costasilva)
                .title( "Feira Modelo Costa e Silva")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("6ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(tresfigueiras)
                .title( "Feira Ecológica Três Figueiras")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Sábado, das 8h às 13h."));

        mMap.addMarker(new MarkerOptions().position(prodipiranga)
                .title( "Mercadão do Produtor - Vila Ipiranga")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Domingo, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(cavalhada)
                .title( "Mercadão do Produtor - Cavalhada")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Domingo, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(prodcidadeb)
                .title( "Mercadão do Produtor - Cidade Baixa")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Sábado, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(sebastiao)
                .title(  "Mercadão do Produtor - São Sebastião")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "5ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(medianeira)
                .title( "Mercadão do Produtor - Medianeira")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Sábado, das 7:00 às 12:00"));

        mMap.addMarker(new MarkerOptions().position(stoantonio)
                .title( "Mercadão do Produtor - Santo Antônio")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "3ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(ecopetro)
                .title( "Feira Ecológica Petrópolis")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Semanalmente das 13h às 18h."));

        mMap.addMarker(new MarkerOptions().position(bonifacio)
                .title("Feira Ecológica Bom Fim")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Sábado, das 7:00 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(jdbton)
                .title("Feira Modelo Jardim Botânico")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("6ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(asil)
                .title( "Feira Modelo Marinha do asil")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "6ª feira, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(salso)
                .title("Feira Modelo Jardim do Salso")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Sábado, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(hipica)
                .title("Feira Modelo Hípica")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábados, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(mangueira)
                .title("Feira Modelo Passo da Mangueira")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet( "Sábado, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(puc)
                .title("Feira Modelo Partenon PUC-RS")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(juca)
                .title("Feira Modelo Juca Batista")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(fernando)
                .title("Feira Modelo Fernando Machado")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(vipiranga)
                .title("Feira Modelo Vila Ipiranga")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(fernando)
                .title("Feira Modelo Fernando Machado")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 7:30 às 12:30"));

        mMap.addMarker(new MarkerOptions().position(bomfim)
                .title("Feira Modelo Bom Fim")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 15:30 às 20:30"));

        mMap.addMarker(new MarkerOptions().position(restinga)
                .title("Feira Modelo Restinga")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carrot))
                .snippet("Sábado, das 15:30 às 20:30"));

        //mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
    }

    public void listaFeiras(View view){
        this.finish();
        overridePendingTransition(R.anim.pullin_left, R.anim.pushout_right);
       /* overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
        startActivity(new Intent(getBaseContext(), MainActivity.class));
        Toast.makeText(getApplicationContext(), "Buscando todas as feiras", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

//        Toast.makeText(getApplicationContext(), "...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getApplicationContext(), "Conexão Interrompida!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(getApplicationContext(), location.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "Erro ao conectar!", Toast.LENGTH_SHORT).show();
    }
}
