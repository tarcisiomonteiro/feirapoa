package br.com.feirapoa;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import br.com.feirapoa.model.Usuario;
import br.com.feirapoa.service.PUTService;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public String deviceId;
    private Usuario usuario;
    private GoogleMap mMap;
    private Context mContext;
    private LocationSource.OnLocationChangedListener mListener;
    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        String message = "You are now starting... ";
        new PUTService().execute(deviceId);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
           return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
           return;
        }
        mMap.setMyLocationEnabled(true);

//        LatLng runLocation = new LatLng(mMap.getMyLocation().getAltitude(), mMap.getMyLocation().getLongitude());
        LatLng myLocation = new LatLng(-29.7975587, -51.1507757);
        LatLng secondLoctaion = new LatLng(-29.796790, -51.148920);
        // mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude())));

        if (Build.VERSION.SDK_INT >= 23) {
            requestSinglePermission();
        }

        CameraUpdate update = (CameraUpdateFactory.newLatLngZoom(secondLoctaion, 16));
        mMap.animateCamera(update, 4000, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(), "Location acquired", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Location lost, check your settings", Toast.LENGTH_LONG).show();
            }
        });

        mMap.addMarker(new MarkerOptions()
                .position(secondLoctaion).title("Running")
                .snippet("First Run"));

    }

    //Authorization code for Marshmallow version.
    private void requestSinglePermission() {
        int REQUEST_LOCATION = 1503;
        String locationPermission = Manifest.permission.ACCESS_FINE_LOCATION;
        int hasPermission = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasPermission = checkSelfPermission(locationPermission);
        }
        String[] permissions = new String[]{locationPermission};
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, REQUEST_LOCATION);
            }
        } else {
            // Phew - we already have permission!
        }
    }

    public void startChronometer(View view) {
        ((Chronometer) findViewById(R.id.chronometer)).start();

        String message =  "You are now starting... ";
        new PUTService().execute();
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void stopChronometer(View view) {
        ((Chronometer) findViewById(R.id.chronometer)).stop();
    }

    public void endChronometer(View view){
        new PUTService().kill();
        this.finish();
        overridePendingTransition(R.anim.pullin_left, R.anim.pushout_right);
       /* overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
        startActivity(new Intent(getBaseContext(), MainActivity.class));
        Toast.makeText(getApplicationContext(), "Your time was: 14 min ", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart(){
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop(){
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onLocationChanged(Location location) {

        mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(location.getLatitude(), location.getLongitude()))
                .width(6)
                .color(Color.BLUE));
    }

    @Override
    public void onConnected(Bundle bundle) {

        Toast.makeText(getApplicationContext(), "Conectado ao Goole Play!!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getApplicationContext(), "Conexão Interrompida!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "Erro ao conectar!", Toast.LENGTH_SHORT).show();
    }


    //method to find the location via GooglePlay
    private void setMapaLocation(Location l){
        if (mMap != null && l!= null){
            LatLng latlng = new LatLng(l.getLatitude(), l.getLongitude());
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latlng, 15);
            mMap.animateCamera(update);

            Log.d("", "location" + l);

 /*           TextView text = (TextView) findViewById(R.id.textLoca);
            text.setText(String.format("Lat/Lnt %f/%f, provider: %s", l.getLatitude(), l.getLongitude(), l.getProvider()));

*/
            CircleOptions circle = new CircleOptions().center(latlng);
            circle.fillColor(Color.BLUE);
            circle.radius(25);
            mMap.clear();
            mMap.addCircle(circle);
        }
    }
}