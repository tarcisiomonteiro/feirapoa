package br.com.feirapoa.service;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.feirapoa.R;
import br.com.feirapoa.model.Feira;

/**
 * Created by Rafael on 22/04/2016.
 */
public class FeiraService {

    private static final boolean LOG_ON = false;
    private static final String TAG = "FeiraService";

    public static List<Feira> getFeiras(Context context, String feira){
        try{
            String json = readFile(context, feira);
            List<Feira> feiras = parserJSON(context, json);
            return feiras;

        }catch (Exception e){
            Log.e(TAG, "Erro ao ler as feiras: " + e.getMessage());
            return null;
        }
    }

    private static List<Feira> parserJSON(Context context, String json) {
        List<Feira> feiras = new ArrayList<Feira>();
        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("feiras");
            JSONArray jsonFeiras = obj.getJSONArray("feira");
            for (int i =0; i < jsonFeiras.length(); i++){
                JSONObject jsonFeira = jsonFeiras.getJSONObject(i);

                Feira f = new Feira();
                f.titulo = jsonFeira.optString("titulo");
                f.endereco = jsonFeira.optString("endereço");
                f.horario = jsonFeira.optString("horario");
                f.localizacao = jsonFeira.optString("localizacao");
                if (LOG_ON){
                    Log.d(TAG, feiras.size() + "encontradas");
                }
                feiras.add(f);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return feiras;
    }

    private static String readFile(Context context, String feira) throws IOException {
        return FileUtils.readRawFileString(context, R.raw.feiras, "UTF-8");
    }

}
